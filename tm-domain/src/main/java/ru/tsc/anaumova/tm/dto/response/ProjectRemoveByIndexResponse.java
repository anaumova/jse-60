package ru.tsc.anaumova.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.anaumova.tm.dto.model.ProjectDto;

@NoArgsConstructor
public final class ProjectRemoveByIndexResponse extends AbstractProjectResponse {

    public ProjectRemoveByIndexResponse(@Nullable final ProjectDto project) {
        super(project);
    }

}