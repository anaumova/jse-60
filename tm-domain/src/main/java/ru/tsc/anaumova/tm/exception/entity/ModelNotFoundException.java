package ru.tsc.anaumova.tm.exception.entity;

import ru.tsc.anaumova.tm.exception.AbstractException;

public final class ModelNotFoundException extends AbstractException {

    public ModelNotFoundException() {
        super("Error! Model not found...");
    }

}