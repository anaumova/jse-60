package ru.tsc.anaumova.tm.api.repository.dto;

import ru.tsc.anaumova.tm.dto.model.ProjectDto;

public interface IProjectDtoRepository extends IUserOwnedDtoRepository<ProjectDto> {
}
