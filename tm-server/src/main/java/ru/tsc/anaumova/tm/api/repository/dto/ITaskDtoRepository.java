package ru.tsc.anaumova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.anaumova.tm.dto.model.TaskDto;

import java.util.List;

public interface ITaskDtoRepository extends IUserOwnedDtoRepository<TaskDto> {

    @NotNull
    List<TaskDto> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeTasksByProjectId(String projectId);

}