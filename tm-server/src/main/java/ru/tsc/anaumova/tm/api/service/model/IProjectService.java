package ru.tsc.anaumova.tm.api.service.model;

import ru.tsc.anaumova.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {
}
