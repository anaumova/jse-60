package ru.tsc.anaumova.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.anaumova.tm.api.service.IAuthService;
import ru.tsc.anaumova.tm.api.service.IPropertyService;
import ru.tsc.anaumova.tm.api.service.dto.IUserDtoService;
import ru.tsc.anaumova.tm.dto.model.SessionDto;
import ru.tsc.anaumova.tm.dto.model.UserDto;
import ru.tsc.anaumova.tm.enumerated.Role;
import ru.tsc.anaumova.tm.exception.field.EmptyLoginException;
import ru.tsc.anaumova.tm.exception.field.EmptyPasswordException;
import ru.tsc.anaumova.tm.exception.system.AccessDeniedException;
import ru.tsc.anaumova.tm.util.CryptUtil;
import ru.tsc.anaumova.tm.util.HashUtil;

import java.util.Date;

@Service
public class AuthService implements IAuthService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IUserDtoService userService;

    @NotNull
    @Override
    public String login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final UserDto user = userService.findByLogin(login);
        if (user == null || user.getLocked()) throw new AccessDeniedException();
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        @NotNull String hash = HashUtil.salt(password, secret, iteration);
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        return getToken(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public SessionDto validateToken(@Nullable final String token) {
        if (token == null) throw new AccessDeniedException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull final String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException(e);
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final SessionDto session = objectMapper.readValue(json, SessionDto.class);
        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getDate();
        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        final int timeout = propertyService.getSessionTimeout();
        if (delta > timeout) throw new AccessDeniedException();
        return session;
    }

    @NotNull
    @Override
    public UserDto registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        return userService.create(login, password, email);
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final UserDto user) {
        return getToken(createSession(user));
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final SessionDto session) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    @NotNull
    private SessionDto createSession(@NotNull final UserDto user) {
        @NotNull final SessionDto session = new SessionDto();
        session.setUserId(user.getId());
        @NotNull final Role role = user.getRole();
        session.setRole(role);
        return session;
    }

}